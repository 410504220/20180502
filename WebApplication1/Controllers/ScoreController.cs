﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int Score)
        {
            
   
            string level = "";
            if ( 80<=Score&& Score <= 100)
            {
                level = "A";
            }
            else if (60 <= Score && Score <= 79)
            {
                level = "B";
            }
            else if (40 <= Score && Score <= 59)
            {
                level = "C";
            }
            else if (20 <= Score && Score <= 39)
            {
                level = "D";
            }
            
            else if (0 <= Score && Score <= 19)
            {
                level = "E";
            }
            ViewBag.Score = Score;
            ViewBag.level = level;
            return View();
        }
    }
}